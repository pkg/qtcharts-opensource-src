qtcharts-opensource-src (5.15.8-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 01 Apr 2023 04:48:48 +0000

qtcharts-opensource-src (5.15.8-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 13 Jan 2023 12:01:37 +0400

qtcharts-opensource-src (5.15.8-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.8.
  * Bump Standards-Version to 4.6.2, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 07 Jan 2023 17:36:47 +0400

qtcharts-opensource-src (5.15.7-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 17 Dec 2022 18:20:07 +0300

qtcharts-opensource-src (5.15.7-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.7.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 04 Dec 2022 20:50:50 +0300

qtcharts-opensource-src (5.15.6-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 29 Sep 2022 11:55:47 +0300

qtcharts-opensource-src (5.15.6-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.6.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 13 Sep 2022 13:29:29 +0300

qtcharts-opensource-src (5.15.5-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.5.
  * Use symver directive to catch all private symbols at once.
    - Fixes build with LTO enabled (closes: #1015632).

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 30 Jul 2022 20:09:06 +0300

qtcharts-opensource-src (5.15.4-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 13 Jun 2022 21:36:17 +0300

qtcharts-opensource-src (5.15.4-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.4.
  * Bump Standards-Version to 4.6.1, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 14 May 2022 12:56:21 +0300

qtcharts-opensource-src (5.15.3-1) experimental; urgency=medium

  * New upstream release.
  * Update debian/watch.
  * Bump Qt build-dependencies to 5.15.3.
  * Update email address for Patrick Franz.
  * Bump Standards-Version to 4.6.0, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 06 Mar 2022 19:25:47 +0300

qtcharts-opensource-src (5.15.2-2co0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 28 Apr 2021 16:49:29 +0200

qtcharts-opensource-src (5.15.2-2) unstable; urgency=medium

  * Bump Standards-Version to 4.5.1, no changes needed.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 11 Dec 2020 11:31:38 +0300

qtcharts-opensource-src (5.15.2-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.2.
  * Build-depend only on the needed documentation tools, not on the
    large qttools5-dev-tools package.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 25 Nov 2020 20:32:28 +0300

qtcharts-opensource-src (5.15.1-3) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 28 Oct 2020 21:53:08 +0300

qtcharts-opensource-src (5.15.1-2) experimental; urgency=medium

  * New upstream release.
    - Bump Qt build dependencies.
  * Use ${DEB_HOST_MULTIARCH} substitution in .install files.
  * Update symbols files with current build log.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Tue, 15 Sep 2020 16:08:40 -0300

qtcharts-opensource-src (5.14.2-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 24 Jun 2020 12:18:13 +0300

qtcharts-opensource-src (5.14.2-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release.
  * Bump Qt build-dependencies to 5.14.2.
  * Remove unneeded injection of linker flags.
  * Bump debhelper-compat from 12 to 13.
  * Remove build paths from .prl files for reproducibility.

  [ Dmitry Shachnev ]
  * Drop dh_missing override, not needed with debhelper compat 13.

 -- Patrick Franz <patfra71@gmail.com>  Mon, 04 May 2020 18:41:32 +0200

qtcharts-opensource-src (5.14.1-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release.
  * Bump Standards-Version to 4.5.0, no changes needed.
  * Bump debhelper-compatibility to 12:
    - Switch debhelper build dependency to debhelper-compat 12.
    - Remove debian/compat.
  * Update the Uploaders-field.
  * Bump Qt build-dependencies to 5.14.1.
  * Add Rules-Requires-Root field.
  * Update debian/copyright.

  [ Dmitry Shachnev ]
  * Update debian/libqt5charts5.symbols from the current build log.
    - Many symbols are now marked as Qt_5_PRIVATE_API because QTBUG-74752
      was fixed.

 -- Patrick Franz <patfra71@gmail.com>  Sun, 15 Mar 2020 21:48:27 +0100

qtcharts-opensource-src (5.12.5-2) unstable; urgency=medium

  * Run tests with QSG_NO_DEPTH_BUFFER=1 to workaround Qt Quick crash on
    mips64el (see bug #868745 for details).

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 22 Oct 2019 21:06:25 +0300

qtcharts-opensource-src (5.12.5-1) unstable; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.12.5.
  * Bump Standards-Version to 4.4.1, no changes needed.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 21 Oct 2019 12:04:09 +0300

qtcharts-opensource-src (5.12.4-1) experimental; urgency=medium

  [ Scarlett Moore ]
  * Update packaging to use doc-base as per policy 9.10.

  [ Dmitry Shachnev ]
  * New upstream release.
  * Bump Qt build-dependencies to 5.12.4.
  * Simplify debian/rules by using a debian/not-installed file.
  * Add Build-Depends-Package field to debian/libqt5charts5.symbols.
  * Make the tests results fatal. Run them with QTEST_ENVIRONMENT=ci,
    to make the failing tst_QBarSeries::mousehovered test blacklisted.
  * Add support for nocheck build profile.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 28 Jun 2019 21:40:04 +0300

qtcharts-opensource-src (5.12.2-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.12.2.
  * Update debian/libqt5charts5.symbols from the current build log.

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 28 Mar 2019 00:27:34 +0300

qtcharts-opensource-src (5.11.3-2) unstable; urgency=medium

  [ Simon Quigley ]
  * Change my email to tsimonq2@debian.org now that I am a Debian Developer.
  * Bump Standards-version to 4.3.0, no changes needed.

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Upload to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 26 Dec 2018 16:50:27 -0300

qtcharts-opensource-src (5.11.3-1) experimental; urgency=medium

  * New upstream release.
   - Bump Qt build dependencies.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Mon, 17 Dec 2018 19:36:44 -0300

qtcharts-opensource-src (5.11.2-2) unstable; urgency=medium

  * Update debian/copyright.
  * Add qtbase5-doc-html to Build-Depends-Indep.
  * Drop explicit build-dependency on libqt5sql5-sqlite.
  * Use https for Homepage URL.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 17 Oct 2018 00:22:05 +0300

qtcharts-opensource-src (5.11.2-1) experimental; urgency=medium

  * New upstream release.
  * Bump Standards-version to 4.2.1, no changes needed.
  * Bump Qt build dependencies to 5.11.2.
  * Bump the copyright year for my contributions.

 -- Simon Quigley <tsimonq2@ubuntu.com>  Sun, 07 Oct 2018 16:50:30 -0500

qtcharts-opensource-src (5.11.1-2) unstable; urgency=medium

  * Upload to Sid.

 -- Simon Quigley <tsimonq2@ubuntu.com>  Wed, 25 Jul 2018 04:49:30 -0500

qtcharts-opensource-src (5.11.1-1) experimental; urgency=medium

  * New upstream release.
  * Bump Standards-version to 4.1.4, no changes needed.
  * Bump debhelper compat to 11, no changes needed.
  * Bump build dependencies to 5.11.1.
  * Update symbols from amd64 build logs.

 -- Simon Quigley <tsimonq2@ubuntu.com>  Mon, 25 Jun 2018 23:22:56 -0500

qtcharts-opensource-src (5.10.1-2) unstable; urgency=medium

  * Switch to using plain dh_auto_configure instead of calling qmake
    (Closes: #888406).
    - Bump debhelper dependency to 10.9.2, required to do the change.
  * Release to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 07 Apr 2018 17:39:50 -0300

qtcharts-opensource-src (5.10.1-1) experimental; urgency=medium

  * New upstream release.
    - Bump Qt build dependencies.
  * Update debian/watch to follow upstream naming changes.
  * Update Vcs-[Git Browser] to use salsa.debian.org repos.
  * Update symbols files with current build log.
  * Update Standards-Version to 4.1.3, no changes required.
  * Simple updates to debian/copyright.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Tue, 06 Mar 2018 13:59:05 -0300

qtcharts-opensource-src (5.9.2-1) unstable; urgency=medium

  * New upstream release.
  * Run the tests, just make the results not fatal for the build.
  * Use dh_missing instead of deprecated dh_install --fail-missing.
  * Stop using deprecated Priority: extra.
  * Bump Standards-Version to 4.1.1.

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 26 Oct 2017 23:49:05 +0300

qtcharts-opensource-src (5.9.1-3) unstable; urgency=medium

  * Disable tests during the transition. Yes, not ideal, but...

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 16 Aug 2017 22:23:41 -0300

qtcharts-opensource-src (5.9.1-2) unstable; urgency=medium

  * Release to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 16 Aug 2017 11:34:26 -0300

qtcharts-opensource-src (5.9.1-1) experimental; urgency=medium

  * New upstream release.
  * Drop fix_license_header.diff, applied in the new release.
  * Bump Qt build-dependencies to 5.9.1.
  * Update debian/copyright.
  * Bump Standards-Version to 4.0.0, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 16 Jul 2017 23:40:20 +0300

qtcharts-opensource-src (5.9.0-1) experimental; urgency=medium

  [ Simon Quigley ]
  * New upstream release.
  * Install new files for candle stick and box plot models.
  * Update symbols from build logs.

  [ Dmitry Shachnev ]
  * Backport upstream patch to fix wrong commercial-only license header
    (fix_license_header.diff, closes: #864158).

 -- Simon Quigley <tsimonq2@ubuntu.com>  Wed, 07 Jun 2017 11:42:27 -0500

qtcharts-opensource-src (5.7.1-3) unstable; urgency=medium

  [ Dmitry Shachnev ]
  * Run the tests only during architecture-specific builds (Closes: #860852).
  * Make qtcharts5-examples depend on qml-module-qtcharts, as there are
    QML examples.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Fri, 21 Apr 2017 14:12:12 -0300

qtcharts-opensource-src (5.7.1-2) unstable; urgency=medium

  * Add missing dependency on libqt5charts5 to libqt5charts5-dev (Closes:
    #860584). Thanks Andreas Beckmann for the bug report.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Thu, 20 Apr 2017 09:18:51 -0300

qtcharts-opensource-src (5.7.1-1) unstable; urgency=medium

  * Initial release (Closes: #697509).

 -- Simon Quigley <tsimonq2@ubuntu.com>  Fri, 14 Apr 2017 19:57:27 -0500
